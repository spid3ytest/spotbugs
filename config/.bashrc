#!/bin/bash -l

update_java_home() {
  local java_path
  java_path="$(asdf which java)"
  if [[ -n "${java_path}" ]]; then
    export JAVA_HOME
   JAVA_HOME="$(dirname "$(dirname "$(realpath "${java_path}")")")"
  fi
}

function switch_to_exact() {
  local tool=$1
  local version=$2

  asdf global "$tool" "$version"
  if [[ "$tool" = "java" ]]; then
    update_java_home
  fi
}

function switch_to() {
  local tool=$1
  local major_version=$2
  local version
  version="$(grep "$tool" "/root/.tool-versions"| tr ' ' '\n' | grep "adoptopenjdk-$major_version.")"

  switch_to_exact "$tool" "$version"
}

function move_tool_versions_to_gl_tmp() {
  if [[ "$SPOTBUGS_MOVE_TOOL_VERSION_FILES" == "yes" ]]; then
    find "$_TARGET" -name .tool-versions -exec mv {} {}.gl-tmp \;
  fi
}

function move_tool_versions_back() {
  if [[ "$SPOTBUGS_MOVE_TOOL_VERSION_FILES" == "yes" ]]; then
    find "$_TARGET" -name .tool-versions.gl-tmp -exec bash -c 'mv $0 ${0%.gl-tmp}' {} \;
  fi
}

# _TARGET is used to move tool version files out of the way if enabled
export _TARGET
if [[ -n "$ANALYZER_TARGET_DIR" ]]; then
  _TARGET=$ANALYZER_TARGET_DIR
elif [[ -n "$CI_PROJECT_DIR" ]]; then
  _TARGET=$CI_PROJECT_DIR
else
  _TARGET=$(pwd)
fi

move_tool_versions_to_gl_tmp

# shellcheck source=/dev/null
. "$ASDF_DATA_DIR/asdf.sh"
# shellcheck source=/dev/null
. "$ASDF_DATA_DIR"/plugins/java/set-java-home.bash
# shellcheck source=/dev/null
. "$ASDF_DATA_DIR"/plugins/ant/set-ant-home.bash
