package main

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/command"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v3"
	"gitlab.com/gitlab-org/security-products/analyzers/spotbugs/v2/directory"
	"gitlab.com/gitlab-org/security-products/analyzers/spotbugs/v2/instance"
	"gitlab.com/gitlab-org/security-products/analyzers/spotbugs/v2/project"
)

func mockMatch(path string, info os.FileInfo) (bool, error) {
	return true, nil
}

func mockConvert(reader io.Reader, prependPath string) (*report.Report, error) {
	return &report.Report{}, nil
}

func newMockApp() *cli.App {
	app := cli.NewApp()
	app.Commands = command.NewCommands(command.Config{
		Match:        mockMatch,
		Analyze:      analyze,
		AnalyzeFlags: analyzeFlags(),
		Convert:      mockConvert,
	})

	return app
}

func TestAnalyzeFlags(t *testing.T) {
	totalFlagsCount := 14
	got := analyzeFlags()
	want := &cli.StringFlag{
		Name:    project.FlagAntPath,
		Usage:   "Define path to ant executable.",
		Value:   "ant",
		EnvVars: []string{"ANT_PATH"},
	}

	require.Equal(t, want, got[0])
	require.Equal(t, totalFlagsCount, len(got))
}

func TestFileNameWithSameInstance(t *testing.T) {
	b1 := instance.Instance{
		InstanceHash: "abcdef1234567890",
		Type:         "find_sec_bugs_type",
		SourceLine: instance.SourceLine{
			Start:      10,
			SourcePath: "abc.txt",
		},
	}

	b2 := instance.Instance{
		InstanceHash: "abcdef1234567890",
		Type:         "find_sec_bugs_type",
		SourceLine: instance.SourceLine{
			Start:      10,
			SourcePath: "abc.txt",
		},
	}

	require.Equal(t, false, fileName(&b1, &b2))
}

func TestFileNameWithStart(t *testing.T) {
	b1 := instance.Instance{
		InstanceHash: "abcdef1234567890",
		Type:         "find_sec_bugs_type",
		SourceLine: instance.SourceLine{
			Start:      10,
			SourcePath: "abc.txt",
		},
	}

	b2 := instance.Instance{
		InstanceHash: "abcdef1234567890",
		Type:         "find_sec_bugs_type",
		SourceLine: instance.SourceLine{
			Start:      20,
			SourcePath: "abc.txt",
		},
	}

	require.Equal(t, true, fileName(&b1, &b2))
}

func TestFileNameWithSourcePath(t *testing.T) {
	b1 := instance.Instance{
		InstanceHash: "abcdef1234567890",
		Type:         "find_sec_bugs_type",
		SourceLine: instance.SourceLine{
			Start:      10,
			SourcePath: "xyz.txt",
		},
	}

	b2 := instance.Instance{
		InstanceHash: "abcdef1234567890",
		Type:         "find_sec_bugs_type",
		SourceLine: instance.SourceLine{
			Start:      10,
			SourcePath: "abc.txt",
		},
	}

	require.Equal(t, false, fileName(&b1, &b2))
	require.Equal(t, true, fileName(&b2, &b1))
}

func TestFileNameWithShortMessage(t *testing.T) {
	b1 := instance.Instance{
		InstanceHash: "abcdef1234567890",
		Type:         "find_sec_bugs_type",
		ShortMessage: "abc",
		SourceLine: instance.SourceLine{
			Start:      10,
			SourcePath: "abc.txt",
		},
	}

	b2 := instance.Instance{
		InstanceHash: "abcdef1234567890",
		Type:         "find_sec_bugs_type",
		ShortMessage: "xyz",
		SourceLine: instance.SourceLine{
			Start:      10,
			SourcePath: "abc.txt",
		},
	}

	require.Equal(t, true, fileName(&b1, &b2))
	require.Equal(t, false, fileName(&b2, &b1))
}

func TestAnalyze(t *testing.T) {
	path := "/tmp"

	app := *newMockApp()
	set := flag.NewFlagSet("testFlagSet", 0)
	set.String("javaPath", "/bin/java", "")
	c := *cli.NewContext(&app, set, nil)

	want := ioutil.NopCloser(bytes.NewReader(([]byte("<Instances></Instances>"))))

	got, err := analyze(&c, path)
	if err != nil {
		t.Fatal(err)
	}

	require.Equal(t, want, got)
}

func TestAnalyzeNoCompile(t *testing.T) {
	path := "/tmp"

	app := *newMockApp()
	set := flag.NewFlagSet("testFlagSet", 0)
	set.String("javaPath", "/bin/java", "")
	c := *cli.NewContext(&app, set, nil)

	set.Bool("compile", false, "compile stuff")

	// We override compile to ensure it is not executed,
	oldCompile := compileProj
	defer func() { compileProj = oldCompile }()

	panicCompile := func(c *cli.Context, projects []project.Project, failNever bool) error {
		return fmt.Errorf("compile should not be called")
	}
	compileProj = panicCompile

	want := ioutil.NopCloser(bytes.NewReader(([]byte("<Instances></Instances>"))))

	got, err := analyze(&c, path)
	if err != nil {
		t.Fatal(err)
	}

	require.Equal(t, want, got)
}

func TestCorrectPathWithProjectRelativePath(t *testing.T) {
	b1 := instance.Instance{
		InstanceHash: "abcdef1234567890",
		Type:         "find_sec_bugs_type",
		ShortMessage: "abc",
		SourceLine: instance.SourceLine{
			Start:      10,
			SourcePath: "App.java",
		},
	}

	b2 := instance.Instance{
		InstanceHash: "abcdef1234567890",
		Type:         "find_sec_bugs_type",
		ShortMessage: "xyz",
		SourceLine: instance.SourceLine{
			Start:      10,
			SourcePath: "abc.txt",
		},
	}

	root := directory.NewDirectory("root", nil)
	components := []string{"subdir", "src", "main", "java", "com", "example", "mypackage", "App.java"}
	root.AddSourceFileComponents(components)

	p := project.Project{
		Path:            filepath.Join("...", "test"),
		SourceFilesTree: root,
	}

	got, err := correctPath("", p, []instance.Instance{b1, b2})
	if err != nil {
		t.Errorf("%s\n", err.Error())
		return
	}

	want := []instance.Instance([]instance.Instance{instance.Instance{Type: "find_sec_bugs_type", CWEID: 0, Rank: 0, Abbrev: "", Priority: 0, InstanceHash: "abcdef1234567890", ShortMessage: "abc", LongMessage: "", Class: struct {
		Name string "xml:\"classname,attr\""
	}{Name: ""}, Method: struct {
		Name string "xml:\"name,attr\""
	}{Name: ""}, SourceLine: instance.SourceLine{Start: 10, End: 0, SourcePath: ".../test/subdir/src/main/java/com/example/mypackage/App.java"}}})

	require.Equal(t, want, got)
}
