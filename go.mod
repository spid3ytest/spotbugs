module gitlab.com/gitlab-org/security-products/analyzers/spotbugs/v2

require (
	github.com/gosimple/slug v1.12.0
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	github.com/termie/go-shutil v0.0.0-20140729215957-bcacb06fecae
	github.com/urfave/cli/v2 v2.3.0
	gitlab.com/gitlab-org/security-products/analyzers/command v1.6.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.24.1
	gitlab.com/gitlab-org/security-products/analyzers/report/v3 v3.8.0
	gitlab.com/gitlab-org/security-products/analyzers/ruleset v1.4.0
)

go 1.15
